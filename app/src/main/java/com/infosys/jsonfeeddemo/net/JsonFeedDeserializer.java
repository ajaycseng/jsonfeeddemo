package com.infosys.jsonfeeddemo.net;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.infosys.jsonfeeddemo.model.JsonFeedModel;
import com.infosys.jsonfeeddemo.model.JsonFeedResponce;

import java.lang.reflect.Type;
import java.util.Map;

public class JsonFeedDeserializer implements JsonDeserializer<JsonFeedResponce> {

    @Override
    public JsonFeedResponce deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonFeedResponce jsonFeedResponce = new JsonFeedResponce();
        if (json.isJsonObject()) {
            for (Map.Entry<String, JsonElement> entry : json.getAsJsonObject().entrySet()) {

                if (entry.getKey().equals("title")) {
                    jsonFeedResponce.setTitle(entry.getValue().getAsString());

                } else if (entry.getKey().equals("rows")) {
                    JsonArray jsonArray = entry.getValue().getAsJsonArray();

                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonFeedModel db = new JsonFeedModel();

                        JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                        db.setMainTitle(String.valueOf(jsonFeedResponce.getTitle()));
                        db.setTitle(String.valueOf(jsonObject.get("title")));
                        db.setDescription(String.valueOf(jsonObject.get("description")));
                        db.setImageHref(String.valueOf(jsonObject.get("imageHref")));


                        jsonFeedResponce.addList(db);

                    }

                }
            }
        }
        return jsonFeedResponce;
    }
}