package com.infosys.jsonfeeddemo.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.infosys.jsonfeeddemo.model.JsonFeedResponce;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class Api {

    private static ApiInterface api;
   // private static final String BASE_URL = "https://api.nytimes.com";
    private static final String BASE_URL = "https://dl.dropboxusercontent.com";



    public static ApiInterface getApi() {
        if (api == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(
                            JsonFeedResponce.class,
                            new JsonFeedDeserializer())
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            api = retrofit.create(ApiInterface.class);
        }
        return api;
    }

    public interface ApiInterface {
        //@GET("/svc/mostpopular/v2/viewed/1.json?api-key=vYVtSOv8A4KQA4GDLfoGhAHd0NBdU6UV")
        @GET("/s/2iodh4vg0eortkl/facts.json")

        Call<JsonFeedResponce> getData();
    }
}