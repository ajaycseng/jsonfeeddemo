package com.infosys.jsonfeeddemo.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;


import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.infosys.jsonfeeddemo.viewmodel.JsonFeedModel;
import com.infosys.jsonfeeddemo.BR;

public class JsonFeedAdapter extends RecyclerView.Adapter<JsonFeedAdapter.GenericViewHolder> {

    private int layoutId;
    private List<com.infosys.jsonfeeddemo.model.JsonFeedModel> jsonFeedModels;
    private JsonFeedModel viewModel;

    public JsonFeedAdapter(@LayoutRes int layoutId, JsonFeedModel viewModel) {
        this.layoutId = layoutId;
        this.viewModel = viewModel;
    }

    private int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return jsonFeedModels == null ? 0 : jsonFeedModels.size();
    }

    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);

        return new GenericViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
        holder.bind(viewModel, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    public void setNYItem(List<com.infosys.jsonfeeddemo.model.JsonFeedModel> jsonFeedModels) {
        this.jsonFeedModels = jsonFeedModels;
    }

    class GenericViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        GenericViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(JsonFeedModel viewModel, Integer position) {
            binding.setVariable(BR.viewModel, viewModel);
            binding.setVariable(BR.position, position);
            binding.executePendingBindings();
        }

    }
}