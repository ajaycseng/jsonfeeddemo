package com.infosys.jsonfeeddemo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.infosys.jsonfeeddemo.R;
import com.infosys.jsonfeeddemo.databinding.ActivityNytimesBinding;
import com.infosys.jsonfeeddemo.viewmodel.JsonFeedModel;

import java.util.List;


public class JSONFeedActivity extends AppCompatActivity {
    private JsonFeedModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nytimes);
        setupBindings(savedInstanceState);
    }



    private void setupBindings(Bundle savedInstanceState) {
        ActivityNytimesBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_nytimes);
        viewModel = ViewModelProviders.of(this).get(JsonFeedModel.class);
        if (savedInstanceState == null) {
            viewModel.init();
        }
        activityMainBinding.setModel(viewModel);
        setupListUpdate();

    }

    private void setupListUpdate() {
        viewModel.loading.set(View.VISIBLE);
        viewModel.fetchList();
        viewModel.getNYItems().observe(this, new Observer<List<com.infosys.jsonfeeddemo.model.JsonFeedModel>>() {
            @Override
            public void onChanged(List<com.infosys.jsonfeeddemo.model.JsonFeedModel> JsonFeedModels) {
                viewModel.loading.set(View.GONE);
                if (JsonFeedModels.size() == 0) {
                    viewModel.showEmpty.set(View.VISIBLE);
                } else {
                    viewModel.showEmpty.set(View.GONE);
                    viewModel.setNyItemInAdapter(JsonFeedModels);
                }
            }
        });
        setupListClick();
    }

    private void setupListClick() {
        viewModel.getSelected().observe(this, new Observer<com.infosys.jsonfeeddemo.model.JsonFeedModel>() {
            @Override
            public void onChanged(com.infosys.jsonfeeddemo.model.JsonFeedModel JsonFeedModel) {
                if (JsonFeedModel != null) {
                    Toast.makeText(JSONFeedActivity.this, "You selected a " + JsonFeedModel.getTitle(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}