package com.infosys.jsonfeeddemo.viewmodel;

import android.view.View;

import java.util.List;

import androidx.databinding.ObservableArrayMap;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infosys.jsonfeeddemo.R;
import com.infosys.jsonfeeddemo.adapter.JsonFeedAdapter;
import com.infosys.jsonfeeddemo.model.JsonFeedResponce;

public class JsonFeedModel extends ViewModel {

    private JsonFeedResponce jsonFeedResponce;
    private JsonFeedAdapter adapter;
    public MutableLiveData<com.infosys.jsonfeeddemo.model.JsonFeedModel> selected;
    public ObservableArrayMap<String, String> images;
    public ObservableInt loading;
    public ObservableInt showEmpty;

    public void init() {
        jsonFeedResponce = new JsonFeedResponce();
        selected = new MutableLiveData<>();
        adapter = new JsonFeedAdapter(R.layout.view_nytime_item, this);
        images = new ObservableArrayMap<>();
        loading = new ObservableInt(View.GONE);
        showEmpty = new ObservableInt(View.GONE);
    }

    public void fetchList() {
        jsonFeedResponce.fetchList();
    }

    public MutableLiveData<List<com.infosys.jsonfeeddemo.model.JsonFeedModel>> getNYItems() {
        return jsonFeedResponce.getNyModel();
    }

    public JsonFeedAdapter getAdapter() {
        return adapter;
    }

    public void setNyItemInAdapter(List<com.infosys.jsonfeeddemo.model.JsonFeedModel> jsonFeedModels) {
        this.adapter.setNYItem(jsonFeedModels);
        this.adapter.notifyDataSetChanged();
    }

    public MutableLiveData<com.infosys.jsonfeeddemo.model.JsonFeedModel> getSelected() {
        return selected;
    }

    public void onItemClick(Integer index) {
        com.infosys.jsonfeeddemo.model.JsonFeedModel db = getItemAt(index);
        selected.setValue(db);
    }

    public com.infosys.jsonfeeddemo.model.JsonFeedModel getItemAt(Integer index) {
        if (jsonFeedResponce.getNyModel().getValue() != null &&
                index != null &&
                jsonFeedResponce.getNyModel().getValue().size() > index) {
            return jsonFeedResponce.getNyModel().getValue().get(index);
        }
        return null;
    }


}