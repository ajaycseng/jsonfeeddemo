package com.infosys.jsonfeeddemo.model;

import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;


import com.infosys.jsonfeeddemo.net.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JsonFeedResponce extends BaseObservable {
    private String title, rows;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRows() {
        return rows;
    }

    public void setRows(String rows) {
        this.rows = rows;
    }

    public List<JsonFeedModel> getNyList() {
        return nyList;
    }

    public void setNyList(List<JsonFeedModel> nyList) {
        this.nyList = nyList;
    }


    private List<JsonFeedModel> nyList = new ArrayList<>();
    private MutableLiveData<List<JsonFeedModel>> nyModel = new MutableLiveData<>();


    public void addList(JsonFeedModel bd) {
        nyList.add(bd);
    }

    public MutableLiveData<List<JsonFeedModel>> getNyModel() {
        return nyModel;
    }

    public void fetchList() {
        Callback<JsonFeedResponce> callback = new Callback<JsonFeedResponce>() {
            @Override
            public void onResponse(Call<JsonFeedResponce> call, Response<JsonFeedResponce> response) {
                JsonFeedResponce body = response.body();
                title = body.title;
                nyModel.setValue(body.nyList);
            }

            @Override
            public void onFailure(Call<JsonFeedResponce> call, Throwable t) {
                Log.e("Test", t.getMessage(), t);
            }
        };

        Api.getApi().getData().enqueue(callback);
    }


}